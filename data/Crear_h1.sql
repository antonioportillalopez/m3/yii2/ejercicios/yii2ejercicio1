﻿DROP DATABASE IF EXISTS h1;
CREATE DATABASE h1
CHARACTER SET utf8
COLLATE utf8_spanish_ci;
USE h1;
  DROP TABLE IF EXISTS entradas;
  CREATE TABLE entradas(
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    texto varchar(255) DEFAULT NULL
  )ENGINE = INNODB;

  INSERT INTO entradas (texto) 
    VALUES(
    'Este texto comienza de ejemplo en el primer registro'),
    ('Aquí va el segundo registro'),
    (''),
    ('esta es el ide 3 porque en el 2 meti un nulo.'),
    ('No escribo más porque me canso.'),
    ('bye, bye'),
    ('adios'),
    ('ciao'),
    ('halo no se como de dice adios en alemán') ;

