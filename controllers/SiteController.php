<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Entradas;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Muestra la página de incio.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    
    public function actionListar1(){
        $s=Entradas::find()->all();
        return $this->render(  //render se envia los argumentos (el archivo_de_la_vista, datos)
                "listarTodos", [ "datos"=>$s,] );
    }

    public function actionListar2(){
        $salida=Entradas::find()->select(['texto'])->asArray()->all();
        return $this->render(  
                "listarTodos", [ "datos"=>$salida,] );
    }

    public function actionListar3(){
        $s=Entradas::find()->select(['texto'])->all();
        return $this->render(  
                "listarTodos", [ "datos"=>$s,] );
    }

    public function actionListar4(){
        $s=new Entradas();
        return $this->render(  
                "listarTodos", [ "datos"=>$s->find()->all(),] );
    }

    public function actionListar5(){
        $s=new Entradas();
        return $this->render(  
                "listarTodos", [ "datos"=>$s->findOne(1)] );
    }

    public function actionListar6(){
        $comandosql=Yii::$app->db->createCommand("Select * from entradas")->queryAll();
        return $this->render(  
                "listarTodos", [ "datos"=>$comandosql] );
        
    }

    public function actionMostrar(){
        $dataProvider= new ActiveDataProvider([
            'query'=>Entradas::find(),
            ]);
   
   
        return $this->render(  
                "mostrar", ['model'=> Entradas::findOne(1)] );
        
    }
}

