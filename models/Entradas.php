<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entradas".
 *
 * @property int $id
 * @property string|null $texto
 */
class Entradas extends \yii\db\ActiveRecord // para crear classes con tablas de base de datos 
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entradas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() // sirve para añadir reglas de negocio 
    {
        return [
            [['texto'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() // son los campos de las tablas
    {
        return [
            'id' => 'ID',
            'texto' => 'Texto',
        ];
    }
}
